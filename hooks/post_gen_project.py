import fileinput
import re

# Remove unnecessary quotes from the .gitlab-ci.yml
for line in fileinput.input(".gitlab-ci.yml", inplace=True):
    print(re.sub('"([^"]+_LIVETEST_HOST)"', r"\1", line), end="")


message = """\
-------------------------------------------------------------------------------

Congratulations, you did it!  Now change into your project directory, and
follow the “Quickstart” instructions in README.md.
"""
print(message)
