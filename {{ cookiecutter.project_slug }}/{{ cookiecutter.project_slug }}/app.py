"""The {{ cookiecutter.project_title }} Flask app factory.

This module provides the :func:`create_app` function to create a new
application instance.
"""

import logging.config
import os
import sys
from typing import Any

import sentry_sdk
from flask import Flask
from flask.logging import default_handler
from sentry_sdk.integrations.flask import FlaskIntegration

from .db import db, migrate
from .main import main as main_blueprint


def create_app(config: str = None, **kwargs: Any) -> Flask:  # noqa: ANN401
    """Create a new {{ cookiecutter.project_title }} app.

    :param config: the full path to the configuration file.  If none is
                   given, it is assumed that the path is defined in the
                   ``{{ cookiecutter.project_slug|upper }}_CONFIG`` environment variable.
                   If that variable is not set, a warning will be issued
                   if no settings have been provided as keyword
                   arguments either.
    :param kwargs: configuration settings for the Flask application that
                   will take precedence over the settings in the
                   configuration file.  Only keys in uppercase are
                   considered.
    """
    # Create application instance and load basic configuration
    app = Flask(__name__.split(".")[0])
    app.config.from_object("{{ cookiecutter.project_slug }}.config")

    # Parse additional settings from keyword arguments
    kwargs_config = filter_config_values(kwargs)

    # Fall back to configuration file from environment variable
    if config is None:
        config = os.environ.get("{{ cookiecutter.project_slug|upper }}_CONFIG")

    # Load configuration file
    if config:
        try:
            app.config.from_pyfile(config)
        except OSError:
            sys.stderr.write(f"Cannot read config file: {config}\n")
            sys.exit(2)
        except:
            sys.stderr.write(f"Cannot load config file: {config}\n")
            raise
    elif not kwargs_config:
        sys.stderr.write("No configuration given, using defaults\n")

    # Override config with values from kwargs
    app.config.update(kwargs_config)

    # Initialize logging
    if "{{ cookiecutter.project_slug|upper }}_LOGGING" in app.config:
        app.logger.removeHandler(default_handler)
        logging.config.dictConfig(app.config["{{ cookiecutter.project_slug|upper }}_LOGGING"])

    # Initialize extensions
    initialize_extensions(app)

    # Register blueprints and return the application object
    register_blueprints(app)
    return app


def filter_config_values(input_dict: dict) -> dict:
    """Filter a dictionary for valid configuration values.

    >>> filter_config_values({"ONE": 1, "two": 2, "THREE": 3, "FO_FOUR": 4})
    {'ONE': 1, 'THREE': 3, 'FO_FOUR': 4}

    :param input_dict: the dictionary to filter.
    :return: a new dictionary with all items from the input dictionary
             that have all-uppercase keys.
    """
    return {key: input_dict[key] for key in input_dict if key.isupper()}


def initialize_extensions(app: Flask) -> None:
    """Initialize extensions on a Flask application instance.

    :param app: a :class:`Flask` application instance
    """
    # Initialize database
    if "SQLALCHEMY_ECHO" not in app.config:
        # Echo SQL queries by default in debug mode
        app.config["SQLALCHEMY_ECHO"] = app.debug
    db.init_app(app)
    migrate.init_app(app)

    # Configure Sentry integration
    if app.config.get("SENTRY_DSN"):
        release = "{{ cookiecutter.project_slug }}@{SENTRY_RELEASE}".format(**app.config)
        sentry_sdk.init(
            dsn=app.config["SENTRY_DSN"],
            release=release,
            environment=app.config["SENTRY_ENVIRONMENT"],
            integrations=[FlaskIntegration()],
        )


def register_blueprints(app: Flask) -> None:
    """Register application blueprints on a Flask application instance.

    :param app: a :class:`Flask` application instance
    """
    url_map = {None: main_blueprint}
    for url_prefix, blueprint in url_map.items():
        app.register_blueprint(blueprint, url_prefix=url_prefix)
