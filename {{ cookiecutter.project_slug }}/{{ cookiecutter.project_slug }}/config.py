"""{{ cookiecutter.project_title }} default settings."""

import importlib.metadata
import os

# Customizable configuration
# ==========================
#
# Consider all settings in this section to be “public”, i.e., consider
# potentially any of them to be configured differently in concrete
# application instances.


# Application settings
# --------------------
#
# Put your own application-specific settings here.

# Configure logging
{{ cookiecutter.project_slug|upper }}_LOGGING = {
    "version": 1,
    "formatters": {
        "default": {
            "format": "%(asctime)s [%(levelname)s] %(name)s: %(message)s",
        },
        "json": {
            "class": "{{ cookiecutter.project_slug }}.logs.CustomJsonFormatter",
            "format": (
                "%(datetime)s %(level)s %(logger)s %(message)s "
                "%(source)s %(lineno)s %(function)s"
            ),
        },
    },
    "handlers": {
        "stream": {
            "class": "logging.StreamHandler",
            "level": "DEBUG",
            "formatter": "default",  # think about using `json` for production
            "stream": "ext://sys.stdout",
        },
    },
    "loggers": {
        "{{ cookiecutter.project_slug }}": {
            "level": "DEBUG",
            "propagate": False,
            "handlers": ["stream"],
        },
        # # Enable SQL statement logging
        # "sqlalchemy.engine": {"level": "INFO"},
    },
    "root": {"level": "INFO", "handlers": ["stream"]},
}

# Flask settings
# --------------
SECRET_KEY = os.environ.get("{{ cookiecutter.project_slug|upper }}_APP_SECRET", "change_me")
SESSION_COOKIE_NAME = "{{ cookiecutter.project_slug }}"


# Flask-SQLAlchemy settings
# -------------------------
SQLALCHEMY_DATABASE_URI = os.environ.get(
    "{{ cookiecutter.project_slug|upper }}_DATABASE_URI",
    "postgresql://postgres@postgres/postgres",
)


# Sentry settings
# ---------------
#: A valid DSN for enabling Sentry integration.  If omitted, Sentry
#: integration will be disabled.
# SENTRY_DSN = "<protocol>://<key>@<host>/<project>"  # noqa: ERA001
SENTRY_ENVIRONMENT = "development"
SENTRY_RELEASE = importlib.metadata.version("{{ cookiecutter.project_slug }}")
SENTRY_TRACES_SAMPLE_RATE = 0.0


# Application-internal configuration
# ==================================
#
# Consider all settings in this section to be “private”, i.e., they
# should never be changed.


# Flask-SQLAlchemy settings
# -------------------------
# Set the ``sqlalchemy.engine`` logger’s log level to ``INFO`` or
# ``DEBUG`` if you want to see the executed SQL statements.
SQLALCHEMY_ECHO = False
SQLALCHEMY_TRACK_MODIFICATIONS = False
