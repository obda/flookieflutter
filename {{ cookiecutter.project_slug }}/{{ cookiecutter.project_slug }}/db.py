"""Database integration via SQLAlchemy.

This module instantiates a `Flask-SQLAlchemy`_ instance and a
`Flask-Migrate`_ instance, both of which must later be initialized with
a Flask application instance:

.. code-block:: python

   db.init_app(app)
   migrate.init_app(app)

.. _Flask-SQLAlchemy: http://flask-sqlalchemy.pocoo.org/
.. _Flask-Migrate: https://flask-migrate.readthedocs.io/
"""

from pathlib import Path

from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

migrations_dir = Path(__file__).parent / "migrations"

db = SQLAlchemy()
migrate = Migrate(db=db, directory=str(migrations_dir))
