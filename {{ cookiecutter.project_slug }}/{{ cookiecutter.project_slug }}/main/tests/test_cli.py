"""Tests for the ``main`` blueprint’s CLI commands."""

from ...tests.base import TestCaseBase
from ..cli import welcome


class TestWelcomeCommand(TestCaseBase):
    """Tests for the “welcome” command."""

    def test_message_contains_project_title(self) -> None:
        """Check that the project title is part of the welcome message."""
        result = self.cli_runner.invoke(welcome)
        assert "{{ cookiecutter.project_title|replace('"', '\\"') }}" in result.output

    def test_message_greets_world_by_default(self) -> None:
        """Check that the whole World is greeted by default."""
        result = self.cli_runner.invoke(welcome)
        assert result.output.endswith("World!\n")

    def test_message_greets_provided_name_by_default(self) -> None:
        """Check that the provided “name” argument is greeted."""
        name = "Monty"
        result = self.cli_runner.invoke(welcome, [name])
        assert result.output.endswith(f"{name}!\n")
