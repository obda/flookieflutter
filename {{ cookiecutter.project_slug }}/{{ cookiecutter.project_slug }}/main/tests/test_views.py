"""Tests for the ``main`` blueprint’s views."""

from http import HTTPStatus
from typing import Any

from flask import Flask

from ...tests.base import TestCaseBase


class TestIndexView(TestCaseBase):
    """Tests for the index view."""

    def test_index_welcome_message(self) -> None:
        """Check that the index view returns a plain text welcome message."""
        response = self.client.get("/")
        message = response.get_data().decode()
        assert response.mimetype == "text/plain"
        assert "{{ cookiecutter.project_title|replace('"', '\\"') }}" in message


class TestHealthcheck(TestCaseBase):
    """Tests for the healthcheck view."""

    def get_default_response(
        self,
        **kwargs: Any,  # noqa: ANN401
    ) -> Flask.response_class:
        """Call the view without parameters, and return the response."""
        return self.client.get("/.well-known/healthcheck", **kwargs)

    def test_json_content_type(self) -> None:
        """Verify that the response uses the JSON content type."""
        response = self.get_default_response()
        assert response.mimetype == "application/json"

    def test_json_content_type_accept_xhtml(self) -> None:
        """Verify that JSON is returned even when not accepted explicitly."""
        response = self.get_default_response(
            headers=[("accept", "text/html,application/xhtml+xml")],
        )
        assert response.mimetype == "application/json"

    def test_http_status_code_ok(self) -> None:
        """Check that the response is served with HTTP status “OK”."""
        response = self.get_default_response()
        assert response.status_code == HTTPStatus.OK

    def test_response_contains_status(self) -> None:
        """Verify that the response contains status information."""
        response = self.get_default_response()
        data = response.get_json()
        assert data["status"] == "OK"
