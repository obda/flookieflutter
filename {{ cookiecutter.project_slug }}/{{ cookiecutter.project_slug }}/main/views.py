"""Views for the ``main`` blueprint."""

from flask import Response, current_app

from .blueprint import main


@main.route("/")
def index() -> Response:
    """Show a plain text welcome message."""
    message = "This is {{ cookiecutter.project_title|replace('"', '\\"') }}!\n"
    return Response(message, mimetype="text/plain")


@main.route("/.well-known/healthcheck")
def healthcheck() -> dict:
    """Return status information (as JSON), usable for health checks."""
    version = current_app.config["SENTRY_RELEASE"]
    return {"status": "OK", "version": version}
