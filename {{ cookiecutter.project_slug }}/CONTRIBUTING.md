Contributing
============

Updating dependencies
---------------------

To update a package to the latest available version, run:

    ./do poetry add [--dev] <package>@latest


Updating the FlookieFlutter-based Skeleton
------------------------------------------

Run the following from the project root to render the current FlookieFlutter
template into a temporary project directory:

    cookiecutter \
        --config-file cookiecutter.yaml \
        --no-input \
        --output-dir /tmp/{{ cookiecutter.project_slug }}-update \
        https://gitlab.com/obda/flookieflutter.git

Afterwards, invoke `diff` and manually merge the changes:

    diff -r -u $(pwd) /tmp/{{ cookiecutter.project_slug }}-update/{{ cookiecutter.project_slug }}

When you are finished, remove the temporary directory:

    rm -r /tmp/{{ cookiecutter.project_slug }}-update
