Contributing
============

Run the following from the project root to test this cookiecutter:

    cookiecutter \
        --output-dir ./build \
        --config-file ./tests/my_test_project.yml \
        --no-input \
        .

Afterwards, change into the `build/my_test_project` directory and check out
the generated test project.
